\#은 Header를 의미합니다.

# H1 tag

## H2 tag

### H3 tag

### H4 tag

\#를 5개 이상 쓴다는 것? -> 잘못된 분류!!

---

_italic_

**bold**

**_italic + bold_**

~~hello~~

> 안녕하세요 이건 **Quote** 입니다

---

- list 1
- list 2
- list 3
    - A
        - B

1. list
2. list
4. list
    1. A
        2. B

| Title | Description | 비고 | 
| ----- | ----------- | --- |
| 이것은 무엇인가 | 이건 표입니다 | |
| | | |

| | | | |
|-|-|-|-|
| | | | |

---

[링크이름](naver.com)

<a href="naver.com">링크이름</a>

[subinium의 블로그](subinium.github.io)

<img src="https://subinium.github.io/assets/images/mac_asb.jpg" width="100">

![subinium](https://subinium.github.io/assets/images/mac_asb.jpg)

---

`simple code`

``` cpp
#include <stdio.h>

int main(){
    printf("Hello, World!");
    return 0;
}
```

``` java
package com.test.hello;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }
}
```

``` sh
Hello, World!
```

---

$a+b=c$

$a^2_1$

$ a^2 + b^2 = c^2 $

다음 중 $N$개를 고르시오

$$ (\alpha + \beta)^2 = \alpha^2 + 2\alpha\beta + \beta^2 $$

$$min_G*max_D$$

---

``` mermaid
graph LR
    1-->2
    1-->4
    2-->3
    3-->2
```