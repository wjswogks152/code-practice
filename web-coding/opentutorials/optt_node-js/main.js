const http = require('http');
const fs = require('fs');
const app = http.createServer(function (request, response) {
  let requestUrl = request.url;
  if (requestUrl == '/') {
    requestUrl = '/index.html';
  }
  if (requestUrl == '/favicon.ico') {
    response.writeHead(404);
    response.end();
    return;
  }
  response.writeHead(200);
  response.end(fs.readFileSync(__dirname + requestUrl));
});
app.listen(3000);
