const jsCanvas = document.getElementById('js-canvas');

function onMouseMove(event) {
  console.log(event);
}

function init() {
  if (jsCanvas) {
    jsCanvas.addEventListener('mousemove', onMouseMove);
  }
}

init();
