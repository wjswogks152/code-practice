from wordcloud import WordCloud

from PIL import Image
import numpy as np

text = ''
with open("kakaotalk.txt", "r", encoding="utf-8") as f:
    lines = f.readlines()
    for line in lines[3:]:
        if not line.isspace():
            if not '-------- 2' in line:
                if not '님을 초대하였습니다.' in line:
                    if not '님이 나갔습니다.' in line:
                        if '] [' in line:
                            text += line.split('] ')[2].replace(
                                'ㅋ', '').replace('ㅠ', '').replace(
                                'ㅜ', '').replace('내일', '').replace(
                                '오늘', '').replace('까지', '').replace(
                                '바랍니다', '').replace('됩니다', '').replace(
                                '합니다', '').replace('이모티콘\n', '').replace(
                                '사진\n', '').replace('삭제된 메시지입니다', '')
                        else:
                            text += line.replace('내일', '').replace(
                                '오늘', '').replace('까지', '').replace(
                                '바랍니다', '').replace('됩니다', '').replace(
                                '합니다', '')

mask = np.array(Image.open('cloud.png'))
wc = WordCloud(
    font_path='C:/Windows/Fonts/NanumGothicBold.ttf',
    background_color="white", mask=mask
)
wc.generate(text)
wc.to_file("homework_result_masked.png")