from bs4 import BeautifulSoup
from selenium import webdriver

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders




# 기사 크롤링해서 저장하기

driver = webdriver.Chrome('chromedriver')

from openpyxl import Workbook

wb = Workbook()
ws1 = wb.active
ws1.title = "homework_articles"
ws1.append(["제목", "링크", "신문사", "썸네일"])

url = f"https://search.naver.com/search.naver?&where=news&query=추석"

driver.get(url)
req = driver.page_source
soup = BeautifulSoup(req, 'html.parser')

articles = soup.select(
    '#main_pack > div.news.mynews.section._prs_nws > ul > li'
)

for article in articles:
    title = article.select_one('dl > dt > a').text
    href = article.select_one('dl > dt > a')['href']
    company = article.select_one(
        'dl > dd.txt_inline > span._sp_each_source'
    ).text.split(' ')[0].replace('언론사', ' ')
    thumbnail = article.select_one('div > a > img')['src']

    ws1.append([title, href, company, thumbnail])

wb.save(filename='homework_articles.xlsx')
driver.quit()




# 파일을 첨부한 이메일 보내기

# 보내는 사람 정보
me = "wjswogks152@gmail.com"
my_password = "JJH-164001189"

# 로그인하기
s = smtplib.SMTP_SSL('smtp.gmail.com')
s.login(me, my_password)

# 받는 사람 정보
you = "wjswogks152@naver.com"

# 메일 기본 정보 설정
msg = MIMEMultipart('alternative')
msg['Subject'] = "숙제 추석"
msg['From'] = me
msg['To'] = you

# 메일 내용 쓰기
content = "숙제 추석"
part2 = MIMEText(content, 'plain')
msg.attach(part2)

# 파일 첨부하기
part = MIMEBase('application', "octet-stream")
with open("homework_articles.xlsx", 'rb') as file:
    part.set_payload(file.read())
encoders.encode_base64(part)
part.add_header('Content-Disposition', "attachment", filename="숙제_추석.xlsx")
msg.attach(part)

# 메일 보내고 서버 끄기
s.sendmail(me, you, msg.as_string())
s.quit()