from wordcloud import WordCloud

from PIL import Image
import numpy as np

text = ''
with open("kakaotalk.txt", "r", encoding="utf-8") as f:
    lines = f.readlines()
    for line in lines[3:]:
        # print(line)
        # text += line
        if not line.isspace():
            if not '-------- 2' in line:
                if not '님을 초대하였습니다.' in line:
                    if not '님이 나갔습니다.' in line:
                        if '] [' in line:
                            text += line.split('] ')[2].replace(
                                'ㅋ', '').replace('ㅠ', '').replace(
                                'ㅜ', '').replace('내일', '').replace(
                                '오늘', '').replace('까지', '').replace(
                                '바랍니다', '').replace('됩니다', '').replace(
                                '합니다', '').replace('이모티콘\n', '').replace(
                                '사진\n', '').replace('삭제된 메시지입니다', '')
                        else:
                            text += line.replace('내일', '').replace(
                                '오늘', '').replace('까지', '').replace(
                                '바랍니다', '').replace('됩니다', '').replace(
                                '합니다', '')

# print(text)

# wc = WordCloud(
#     font_path='C:/Windows/Fonts/NanumGothicBold.ttf',
#     background_color="white", width=600, height=400
# )
# wc.generate(text)
# wc.to_file("result.png")

mask = np.array(Image.open('cloud.png'))
wc = WordCloud(
    font_path='C:/Windows/Fonts/NanumGothicBold.ttf',
    background_color="white", mask=mask
)
wc.generate(text)
wc.to_file("result_masked.png")




# import matplotlib.font_manager as fm
#
# # 이용 가능한 폰트 중 '고딕'만 선별
# for font in fm.fontManager.ttflist:
#     if 'Gothic' in font.name:
#         print(font.name, font.fname)
