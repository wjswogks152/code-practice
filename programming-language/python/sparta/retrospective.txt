
# 회고

1. 수강 목적: 스파르타 온라인 수업을 신청한 이유

2. 배운 점/잘한 점: 많이 배웠고 스스로 잘했다고 생각하시는 점을 적어주세요!

3. 보완/발전하고 싶은 점: 수강하는 과정에서 스스로를 보완하고 발전하고 싶은 점을 적어주세요

4. 앞으로의 계획: 이번 수업이 인생 마지막 코딩..아니시죠?
               앞으로의 계획과 목표를 적어주세요 :-)