import dload

from bs4 import BeautifulSoup
from selenium import webdriver
import time

driver = webdriver.Chrome('chromedriver')
driver.get(
    "https://search.daum.net/search?w=img&nil_search=btn&DA=NTB&enc=utf8&q=%EC%95%84%EB%A6%B0"
)
time.sleep(5)

req = driver.page_source
soup = BeautifulSoup(req, 'html.parser')

thumbnails = soup.select('#imgList > div > a > img')

i = 1
for thumbnail in thumbnails:
    img_src = thumbnail['src']
    dload.save(img_src, f'imgs_homework/{i}.jpg')
    i += 1

driver.quit()
