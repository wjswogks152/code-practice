
# 변수, 자료형, 조건문, 반복문, 기타 라이브러리
#
# a = 2
# a = '2'
# b = 3
#
# print(a + b)
#
# first_name = 'jaehan'
# last_name = 'jeon'
#
# print(first_name + last_name)
# print(a + first_name)
# print(str(a) + first_name)
#
# a_list = ['수박', '참외', '배', '사과', '감', '배']
# b_list = ['영희', '철수', ['사과', '감']]
#
# print(a_list[1])
# print(b_list[0])
# print(b_list[2][0])
#
# a_list.append('수박')
#
# print(a_list)
#
# a_dict = {'name': 'bob', 'age': 24}
# a_dict['height'] = 178
# a_dict['fruits'] = a_list
#
# print(a_dict['name'])
# print(a_dict)
# print(a_dict['fruits'][0])
#
# age = 24
#
# if age > 20:
#     print('성인입니다')
# else:
#     print('청소년입니다')
#
# fruits = ['사과','배','배','감','수박','귤','딸기','사과','배','수박']
#
# count = 0
# for fruit in fruits:
#     print(fruit)
#     if fruit == '사과' or fruit == '배':
#         print(fruit)
#         count += 1
#
# print(count)
#
# people = [{'name': 'bob', 'age': 20},
#           {'name': 'carry', 'age': 38},
#           {'name': 'john', 'age': 7},
#           {'name': 'smith', 'age': 17},
#           {'name': 'ben', 'age': 27}]
#
# for person in people:
#     if person['age'] > 20:
#         print(person['name'])
#
# myEmail = 'sparta@naver.com'
# result = myEmail.split('@')[1].split('.')[0]
# result = myEmail.replace('naver', 'gmail')
#
# print(myEmail)
# print(result)