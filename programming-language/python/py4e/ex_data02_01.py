fname = input("Enter file name: ")

try :
    fh = open(fname)
except :
    print("File cannot be opened:", fname)
    quit()

count = 0
tot = 0.0
for line in fh :
    lx = line.rstrip()
    # print(lx.upper()) # 대문자
    if lx.startswith("X-DSPAM-Confidence:") :
        # print(lx)
        count = count + 1                       # print(count)
        ipos = lx.find(":")                     # print(ipos)
        piece = lx[ipos+1:]                     # print(piece)
        value = float(piece)                    # print(value)
        tot = tot + value                       # print(tot)
# print(count)

print("Average spam confidence:", round(tot/count, 12))



