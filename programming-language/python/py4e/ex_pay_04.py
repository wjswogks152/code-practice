sh = input("hours : ")
srph = input("rate per hour : ")

try :
    fh = float(sh)
    frph = float(srph)
except :
    print("Error, please enter numeric input.")    
    quit()

if fh > 40 :
    rpay = fh * frph
    opay = ( fh - 40 ) * ( frph * 0.5 )
    pay = rpay + opay
else :
    pay = fh * frph

print("your pay :", pay)