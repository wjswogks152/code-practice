fname = input("Enter file name : ")

try :
    fh = open(fname)
except :
    print("File cannot be opened :", fname)
    quit()

count = 0
for line in fh :
    line_rst = line.rstrip()
    # if not 'From' in line_rst :
    #    continue
    wds = line_rst.split()
    # Guardian
    if len(wds) < 3 or wds[0] != 'From' : 
        continue
    count = count + 1
    print(wds[1])

print("There were", count, "lines in the file with From as the first word")
