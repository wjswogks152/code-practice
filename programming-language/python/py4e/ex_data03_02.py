fname = input("Enter file name : ")

try :
    fh = open(fname)
except :
    print("File cannot be opened :", fname)
    quit()

fr = fh.read()               # lst = list()            
frs = fr.split()             # for line in fh :
lst = list()                 #     line_rst = line.rstrip()   
for line in frs :            #     wds = line.split()
    lst.append(line)         #     lst = lst + wds
    continue        

lst_set = list(set(lst))    # set() : removing duplicated, unordered

print(sorted(lst_set))

# lst_set.sort()
# print(lst_set) 
         


