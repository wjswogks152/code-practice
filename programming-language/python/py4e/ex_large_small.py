largest = None
smallest = None

while True :
    num = input("Enter a number : ")
    
    if num == "done" : 
        break
    
    try :
        innum = int(num)
    except :
        print("Invalid input")
        continue    
    
    if largest is None :  
        largest = innum
        # print(largest)
    elif largest < innum :
        largest = innum
        # print(largest)
    
    if smallest is None :
        smallest = innum
        # print(smallest)
    elif smallest > innum :
        smallest = innum
        # print(smallest)    
    
print("Maximum is", largest)
print("Minimum is", smallest)