interface Search {
  search(): void;
}

class SearchAll implements Search {
  public search(): void {
    console.log('SEARCH ALL');
  }
}
