class Simple {
  private static instance: Simple;

  private constructor() {}

  public static getInstance(): Simple {
    return this.instance || (this.instance = new this());
  }
}
