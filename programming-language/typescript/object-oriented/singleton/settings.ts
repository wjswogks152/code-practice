export class Settings {
  private static settings: Settings;

  private constructor() {}

  public static getSettings(): Settings {
    // if (this.settings == null) {
    //   this.settings = new Settings();
    // }
    return this.settings || (this.settings = new this());
  }

  private darkMode: boolean = false;
  private fontSize: number = 13;

  public getDarkMode(): boolean {
    return this.darkMode;
  }
  public getFontSize(): number {
    return this.fontSize;
  }

  public setDarkMode(paramDarkMode: boolean): void {
    this.darkMode = paramDarkMode;
  }
  public setFontSize(paramFontSize: number): void {
    this.fontSize = paramFontSize;
  }
}
