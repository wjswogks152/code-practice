import { Settings } from './settings';

export class FirstPage {
  private settings: Settings = Settings.getSettings();

  public setAndPrintSettings(): void {
    this.settings.setDarkMode(true);
    this.settings.setFontSize(15);

    console.log(
      `first page : ${this.settings.getDarkMode} ${this.settings.getFontSize}`
    );
  }
}
