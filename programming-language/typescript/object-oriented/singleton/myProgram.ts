import { FirstPage } from './firstPage';
import { SecondPage } from './secondPage';

class MyProgram {
  public static main(args: String[]): void {
    new FirstPage().setAndPrintSettings();
    new SecondPage().printSettings();
  }
}
