import { Settings } from './settings';

export class SecondPage {
  private settings: Settings = Settings.getSettings();

  public printSettings(): void {
    console.log(
      `second page : ${this.settings.getDarkMode} ${this.settings.getFontSize}`
    );
  }
}
